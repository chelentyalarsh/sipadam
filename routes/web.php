<?php

use App\Http\Controllers\Admin\PemasanganController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GeoJsonController;
use App\Http\Controllers\JaringanPipaController;
use App\Http\Controllers\PelaporanController;
use App\Http\Controllers\PemasanganController as PublicPemasanganController;
use App\Http\Controllers\PelaporanController as PublicPelaporanController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TitikAssesorisController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'frontend/index')->name('home');

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate'])->name('authenticate');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

// Route::get('/register', [RegisterController::class, 'index'])->name('register');
// Route::post('/register', [RegisterController::class, 'store'])->name('createUser');


//Post
// Route::get('posts', [PostController::class, 'index']);
// Route::get('posts/create', [PostController::class, 'create']);
// Route::post('posts', [PostController::class, 'store']);
// Route::get('posts/{post}', [PostController::class, 'show']);
// Route::get('posts/{post}/edit', [PostController::class, 'edit']);
// Route::match(['put', 'patch'],'posts/{post}', [PostController::class, 'update']);
// Route::delete('posts/{post}', [PostController::class, 'destroy']);

Route::resource('posts', PostController::class);

//titikAssesoris
// Route::get('titikAssesoris', [TitikAssesorisController::class, 'index']);
// Route::get('titikAssesoris/create', [TitikAssesorisController::class, 'create']);
// Route::post('titikAssesoris', [TitikAssesorisController::class, 'store']);
// Route::get('titikAssesoris/{titikAssesoris}', [TitikAssesorisController::class, 'show']);
// Route::get('titikAssesoris/{titikAssesoris}/edit', [TitikAssesorisController::class, 'edit']);
// Route::match(['put', 'patch'],'titikAssesoris/{titikAssesoris}', [TitikAssesorisController::class, 'update']);
// Route::delete('titikAssesoris/{titikAssesoris}', [TitikAssesorisController::class, 'destroy']);


//JaringanPipa
Route::resource('jaringan-pipa', JaringanPipaController::class);

Route::get('geojson/{table}', [GeoJsonController::class, 'geojson']);
Route::get('peta', [GeoJsonController::class, 'peta']);

//TitikAssesoris
Route::resource('titik-assesoris', TitikAssesorisController::class);

Route::get('/pemasangan', [PublicPemasanganController::class, 'create'])->name('createPemasangan');
// Route::get('/pelaporan', [PublicPelaporanController::class, 'create'])->name('createPelaporan');
// Admin Routes
Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('pemasangan', PemasanganController::class);
    // Route::resource('pelaporan', PelaporanController::class);
    //Route::resource('titik-assesoris', TitikAssesorisController::class);
});



<?php

namespace App\Helper;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Helper {
    public static function getGeoJson(string $table)
    {
        try {
            $result = DB::table($table, 't')
                ->selectRaw("json_build_object('type', 'FeatureCollection', 'features', json_agg(st_asgeojson(t.*)::json)) as geojson")
                ->whereNotNull('geom')
                ->first();
            return json_decode($result->geojson, true);
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function getColumns(string $table)
    {
        return Schema::getColumnListing($table);
    }

    public static function modelToGeoJson(Model $model)
    {
        try {
            $table = $model->getTable();
            $keyName = $model->getKeyName();
            $key = $model->getKey();
            $record = DB::table($table, 't')->selectRaw('st_asgeojson(t.*) as geojson')
                ->where($keyName, $key)
                ->first();
            return json_decode($record->geojson, true);
        } catch (\Throwable $th) {
            logger()->error($th->getMessage());
        }
    }
    
}

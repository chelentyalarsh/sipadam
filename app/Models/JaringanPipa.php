<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JaringanPipa extends Model
{
    //use HasFactory;
    protected $table = 'update_september21';
    
    protected $guarded = [];

    protected $primaryKey = 'gid';

    public $timestamps = false;
}

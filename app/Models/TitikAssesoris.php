<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TitikAssesoris extends Model
{
    // use HasFactory;
    protected $table = 'titik_assesoris';
    
    protected $guarded = [];

    protected $primaryKey = 'gid';

    public $timestamps = false;
}

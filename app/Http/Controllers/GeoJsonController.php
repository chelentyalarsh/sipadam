<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use Illuminate\Http\Request;
use DB;

class GeoJsonController extends Controller
{
    public function geojson(string $table)
    {
        try {
            $result = Helper::getGeoJson($table);
            return response()->json($result);
        } catch (\Throwable $th) {
            return abort(500);
        }
    }

    public function peta()
    {
        $layer_aksesoris = Helper::getGeoJson('titik_assesoris');
        $layer_update = Helper::getGeoJson('update_september21');

        return view('public.peta', [
            'layer_aksesoris' => $layer_aksesoris,
            'layer_update' => $layer_update,
        ]);

        // return view('public.peta', compact('layer_aksesoris', 'layer_update'));
    }
}

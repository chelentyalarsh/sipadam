<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        logger($request->all());
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:4', 'max:255', 'confirmed']
        ]);

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'level' => 'user'
            ]);

            if ($user instanceof MustVerifyEmail) {
                $user->sendEmailVerificationNotification();
                $message = 'A verification email has been sent. Please verify your email.';
            } else {
                $user->email_verified_at = now();
                $user->save();
                $message = 'Account has been created. Please login to continue.';
            }

            return redirect()->route('login')->with('message', $message);
        } catch (\Throwable $th) {
            logger()->error($th->getMessage());
            return back()->withInput()->with('error', 'An error occured. Please try again or contact administrator.');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Pemasangan;
use Illuminate\Http\Request;

class PemasanganController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('public.pemasangan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attrs = $request->validate([
            'nama'          => 'string|max:255',
            'no_telphone'   => 'numeric',
            'alamat'        => 'string|max:255',
            'saluran'       => 'numeric',
            'kegunaan_persil' => 'string',
            'daya' => 'numeric',
        ]);

        try {
            Pemasangan::create($attrs);
            return response('Data berhasil di tambahkan!', 200);
        } catch (\Throwable $th) {
            //throw $th;
            abort(404, 'Terjadi kesalahan.');
        }
    }
}

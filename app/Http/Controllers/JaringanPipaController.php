<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\JaringanPipa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JaringanPipaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = JaringanPipa::all();
        return view('frontend/jaringanpipa', ['data' => $collection]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new JaringanPipa;
        return view('jaringanpipa.create', compact(
            'model'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:52'],
            'geom' => ['required', 'json'] // GeoJSON geometry
        ]);

        $model = new JaringanPipa;
        $model->name = $request->name;
        $model->kml_folder = $request->kml_folder;
        $model->diameter = $request->diameter;
        $model->panjang = $request->panjang;
        $model->tahun = $request->tahun;
        $model->_lokasi = $request->_lokasi;
        $model->geom = DB::raw("st_setsrid(st_multi(st_geomfromgeojson('$request->geom')), 4326)");
        $model->save();

        return redirect('jaringan-pipa');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JaringanPipa  $jaringan_pipa
     * @return \Illuminate\Http\Response
     */
    public function show(JaringanPipa $jaringan_pipa)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JaringanPipa  $jaringan_pipa
     * @return \Illuminate\Http\Response
     */
    public function edit(JaringanPipa $jaringan_pipa)
    {
       
        return view('frontend/jaringanpipa/edit',['model'=>$jaringan_pipa, 'geojson'=>Helper::modelToGeoJson($jaringan_pipa)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JaringanPipa  $jaringan_pipa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JaringanPipa $jaringan_pipa)
    {
      
        $jaringan_pipa->gid = $request->gid;
        $jaringan_pipa->name = $request->name;
        $jaringan_pipa->kml_folder = $request->kml_folder;
        $jaringan_pipa->diameter = $request->diameter;
        $jaringan_pipa->panjang = $request->panjang;
        $jaringan_pipa->tahun = $request->tahun;
        $jaringan_pipa->_lokasi = $request->_lokasi;
        $jaringan_pipa->geom = $request->geom;
        $jaringan_pipa->save();

        return redirect('jaringan-pipa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JaringanPipa  $jaringan_pipa
     * @return \Illuminate\Http\Response
     */
    public function destroy(JaringanPipa $jaringan_pipa)
    {
       
        $jaringan_pipa->delete();
        return redirect('jaringan-pipa');
    }
}

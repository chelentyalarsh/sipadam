<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pemasangan;
use Illuminate\Http\Request;

class PemasanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pemasangan::all();
        // return \response()->json($data, 200);

        return view('admin.pemasangan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pemasangan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attrs = $request->validate([
            'nama'          => 'string|max:255',
            'no_telphone'   => 'numeric',
            'alamat'        => 'string|max:255',
            'saluran'       => 'numeric',
            'kegunaan persil' => 'string',
            'daya'          => 'string'


        ]);

        try {
            Pemasangan::create($attrs);
            return response('Data berhasil di tambahkan!', 200);
        } catch (\Throwable $th) {
            //throw $th;
            abort(404, 'Terjadi kesalahan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Pemasangan::findOrFail($id);
        return view('admin.pemasangan.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = Pemasangan::findOrFail($id);
        return view('admin.pemasangan.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = Pemasangan::findOrFail($id);
        $attrs = $request->validate([
            'nama'          => 'string|max:255',
            'no_telphone'   => 'numeric',
            'alamat'        => 'string|max:255',
            'saluran'       => 'numeric',
            'kegunaan persil' => 'string',
            'daya'          => 'string'
        ]);

        try {
            $result->forceFill($attrs)->save();
            return response('Data berhasil diperbarui!', 200);
        } catch (\Throwable $th) {
            //throw $th;
            abort(404, 'Terjadi kesalahan.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Pemasangan::findOrFail($id);
        try {
            $result->delete();
            return response('data berhasil dihapus.', 200);
        } catch (\Throwable $th) {
            return abort(500, 'Terjadi kesalahan. Silahkan ulangi atau hubungi administrator.');
        }
    }
}

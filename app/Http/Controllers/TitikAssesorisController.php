<?php

namespace App\Http\Controllers;

use App\Models\TitikAssesoris;
use Illuminate\Http\Request;

class TitikAssesorisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = TitikAssesoris::all();
        return view('frontend/assesoris', ['data' => $collection]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new TitikAssesoris;
        return view('assesoris.create', compact(
            'model'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new TitikAssesoris;
        $model->gid = $request->gid;
        $model->name = $request->name;
        $model->layer = $request->layer;
        $model->gm_type = $request->gm_type;
        $model->kml_folder = $request->kml_folder;
        $model->keterangan= $request->keterangan;
        $model->geom = $request->geom;
        $model->save();

        return redirect('titik-assesoris');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TitikAssesoris  $titik_assesoris
     * @return \Illuminate\Http\Response
     */
    public function show(TitikAssesoris $titik_assesoris)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TitikAssesoris  $titik_assesoris
     * @return \Illuminate\Http\Response
     */
    public function edit(TitikAssesoris $titik_assesoris)
    {
       return view('frontend/assesoris/edit',['model'=>$titik_assesoris]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TitikAssesoris  $titik_assesoris
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TitikAssesoris $titik_assesoris)
    {
       
        $titik_assesoris->gid = $request->gid;
        $titik_assesoris->name = $request->name;
        $titik_assesoris->layer = $request->layer;
        $titik_assesoris->gm_type = $request->gm_type;
        $titik_assesoris->kml_folder = $request->kml_folder;
        $titik_assesoris->keterangan= $request->keterangan;
        $titik_assesoris->geom = $request->geom;
        $titik_assesoris->save();

        return redirect('titik-assesoris');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TitikAssesoris  $titik_assesoris
     * @return \Illuminate\Http\Response
     */
    public function destroy(TitikAssesoris $titik_assesoris)
    {
        $titik_assesoris->delete();
        return redirect('titik-assesoris');
    }
}

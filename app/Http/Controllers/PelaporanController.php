<?php

namespace App\Http\Controllers;

use App\Models\Pelaporan;
use Illuminate\Http\Request;
use Throwable;

class PelaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('public.pelaporan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $attrs = $request->validate([
            'nomor_saluran' => 'numeric',
            'nama_pelapor' => 'string',
            'alamat' => 'string',
            'nomor_telepon' => 'numeric',
            'isi_pelaporan' => 'string',
        ]);

        try {
            Pelaporan::create($attrs);
            return response('Data berhasil ditambahkan', 200);

        } catch (Throwable $th){
            abort(404, 'Terjadi kesalahan.');
        }
    }
}

@extends('auth.layouts.auth')

@section('title')
    Register
@endsection

@section('content')
    <!-- Content -->
    <div class="content text-center">
        <div class="title-text" style="margin-bottom: 20px">
            <h3>Registrasi SI-PADAM</h3>
        </div>

        <form method="POST" action="{{ route('createUser') }}" >
            @csrf
            <!-- Name -->
            <div class="form-group">
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="Nama Asli"
                    required />
            </div>
            <!-- Email -->
            <div class="form-group">
                <input class="form-control" type="email" name="email" value="{{ old('email') }}"
                    placeholder="Alamat Email" required />
            </div>
            <!-- Password -->
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Kata Sandi" required />
            </div>
            <!-- Password -->
            <div class="form-group">
                <input class="form-control" type="password" name="password_confirmation"
                    placeholder="Konfirmasi Kata Sandi" required />
            </div>
            <!-- Submit Button -->
            <button type="submit" class="btn btn-main-md" style="margin-bottom: 15px">Registrasi</button>
        </form>
        <div class="new-acount" style="align-content: initial">
            <p>
                Sudah memiliki akun? <a href="{{ route('login') }}">Masuk SIPADAM</a>
            </p>
        </div>
    </div>
@endsection

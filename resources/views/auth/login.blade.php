@extends('auth.layouts.auth')

@section('title')
    Login
@endsection

@section('content')
    <!-- Content -->
    <div class="content text-center">
        <div class="title-text" style="margin-bottom: 20px">
            <h3>Masuk SI-PADAM</h3>
        </div>

        <form action="{{ route('login') }}" method="POST">
            @csrf

            <!-- Email -->
            <div class="form-group">
                <input class="form-control main" type="email" name="email" value="{{ old('email') }}"
                    placeholder="Alamat Email" required />
            </div>
            <!-- Password -->
            <div class="form-group">
                <input class="form-control main" type="password" name="password" value="{{ old('password') }}"
                    placeholder="Kata Sandi" required />
            </div>
            <div class="form-check" style="margin-bottom: 20px">
                <input class="form-check-input" type="checkbox" name="remember" value="1" id="remember">
                <label class="form-check-label" for="remember">
                    Ingat Saya?
                </label>
            </div>

            <!-- Submit Button -->
            <button class="btn btn-main-md" style="margin-bottom: 15px">Masuk</button>
        </form>
        <div class="new-acount">
            <p>
                {{-- Belum memiliki akun? <a href="{{ route('register') }}">Daftar disini</a> --}}
            </p>
        </div>
    </div>
@endsection

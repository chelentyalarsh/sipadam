@extends('name')

@extends('layout_admin.pelaporan')

@section('title')
    Formulir Pelaporan
@endsection

@section('content')
    <section class="container-fluid">
        <!-- justify-content-center untuk mengatur posisi form agar berada di tengah-tengah -->
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4">
                <form class="form-container">
                    <h4 class="text-center font-weight-bold">Formulir Pelaporan Gangguan Air PDAM</h4>
                    <div class="form-group">
                        <label for="saluran">Nomor Saluran Pelanggan</label>
                        <input type="numeric" class="form-control" id="saluran"
                            placeholder="Masukkan Nomor Saluran Pelanggan" />
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Pelapor</label>
                        <input type="text" class="form-control" id="pelapor" placeholder="Masukkan Nama Pelapor" />
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" />
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor Telepon</label>
                        <input type="numeric" class="form-control" id="nomor" placeholder="Masukkan Nomor Telepon" />
                    </div>
                    <div class="form-group">
                        <label for="pelaporan">Isi Pelaporan</label>
                        <input type="text" class="form-control" id="pelaporan" placeholder="Masukkan Isi Pelaporan" />
                    </div>

                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck" />
                            <label class="form-check-label" for="gridCheck">
                                Apakah data yang diisi sudah benar?
                            </label>
                            <button type="submit" class="btn btn-primary btn-block">
                                Kirim
                            </button>
                            <!-- <div class="form-footer mt-2">
                                      <p>Sudah punya account? <a href="#">Login</a></p>
                                    </div> -->
                </form>
            </section>
        </section>
    </section>
@endsection

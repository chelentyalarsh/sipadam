@extends('frontend.layouts.app')

@section('content')
    <br />
    <form methods="POST" action="{{ route('jaringan-pipa.update', $model->gid) }}">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        GID <input type="text" name="gid" value="{{ $model->gid }}"><br />
        Name <input type="text" name="name" value="{{ $model->name }}"><br />
        KML_folder <input type="text" name="kml_folder" value="{{ $model->kml_folder }}"><br />
        Diameter <input type="text" name="diameter" value="{{ $model->diameter }}"><br />
        Panjang <input type="text" name="panjang" value="{{ $model->panjang }}"><br />
        Tahun <input type="text" name="tahun" value="{{ $model->tahun }}"><br />
        Lokasi <input type="text" name="_lokasi" value="{{ $model->_lokasi }}"><br />
        Geometry
        <textarea name="geom" id="geom" cols="30" rows="5">@json($geojson['geometry'])</textarea>


        <button type="submit">SIMPAN</button>
    </form>

    <div id="map" style="width: 800px; height:400px;"></div>
@endsection

@push('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-draw@1.0.4/dist/leaflet.draw.css">
@endpush

@push('js')
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdn.jsdelivr.net/npm/leaflet-draw@1.0.4/dist/leaflet.draw.min.js"></script>
    <script>
        const geojson = @json($geojson);
        const map1 = L.map('map').setView([-7, 110], 5);

        const basemap1 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> ǀ <a href="PJSIG UGM" target="_blank">SIG UGM</a>' //menambahkan nama//
        });
        basemap1.addTo(map1);

        const layer = L.geoJSON(geojson);
        layer.addTo(map1);
        map1.fitBounds(layer.getBounds());

        const drawControl = new L.Control.Draw({
            draw: false,
            edit: {
                featureGroup: layer,
                remove: false
            }
        });
        map1.addControl(dra
        wControl);

        map1.on('draw:edited', e => {
            const layers = e.layers;
            const geomInput = document.getElementById('geom');
            layers.eachLayer(layer => {
                try {
                    geomInput.value = JSON.stringify(layer.toGeoJSON().geometry);
                } catch (error) {
                    console.error(error);
                    // TODO: Show error
                }
            });
        });
    </script>
@endpush

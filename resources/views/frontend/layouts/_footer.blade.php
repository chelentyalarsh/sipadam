<!--============================
=            Footer            =
=============================-->

<footer>
    <section id="contact" style="background-color: lightblue" style="font-size: 10px">
        <div class="footer-main" style="font-size: 10px">
            <div class="container" style="font-size: 10px">
                <div class="row" style="font-size: 10px">
                    <div class="col-lg-3 col-md-6" style="margin-left: 20px" style="font-size: 10px">
                        <div class="block" style="font-size: 10px">
                            <a href=""><img src="{{ asset('frontend/images/LOGO ID.png') }}" alt="footer-logo"
                                    width="130px" height="45px" style="margin-top: 15px"></a>
                            <p>Jl. Dr. Wahidin Sudiro Husodo No.83, Kabupaten Bantul, DIY
                            </p>

                            <!-- Social Site Icons -->
                            {{-- <ul class="social-icon list-inline">
                                <li class="list-inline-item">
                                    <a href="https://www.facebook.com/themefisher"><i class="ti-facebook"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="https://twitter.com/themefisher"><i class="ti-twitter"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="https://www.instagram.com/themefisher/"><i class="ti-instagram"></i></a>
                                </li>
                            </ul> --}}

                        </div>
                    </div>
                    {{-- <div class="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                        <div class="block-2">
                            <!-- heading -->
                            <h6>Product</h6>
                            <!-- links -->
                            <ul>
                                <li><a href="team.html">Teams</a></li>
                                <li><a href="blog.html">Blogs</a></li>
                                <li><a href="FAQ.html">FAQs</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-lg-3 col-md-6">
                        <div class="block-2">
                            <!-- heading -->
                            <h6>Layanan </h6>
                            <!-- links -->
                            <ul>
                                <li><a href="sign-up.html">Pemasangan</a></li>
                                <li><a href="sign-in.html">Pelaporan</a></li>
                                <li><a href="blog.html">Peta Jaringan Pipa PDAM</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="block-2">
                            <!-- heading -->
                            <h6>Linimasa </h6>
                            <!-- links -->
                            <ul>
                                <li><a href="career.html">Youtube</a></li>
                                <li><a href="contact.html">Email</a></li>
                                <li><a href="team.html">Twitter</a></li>
                                <li><a href="privacy.html">Terms</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4">
                        <div class="block-2">
                            <!-- heading -->
                            <h6>Narahubung </h6>
                            <!-- links -->
                            <ul>
                                <li><a href="career.html"></a></li>
                                <li><a href="contact.html">Nomor Telepon (0274)367524</a></li>
                                <li><a href="team.html">SMS (0811-2848-000)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center bg-dark py-4">
            <small class="text-secondary">Copyright &copy; SI-PADAM Kabupaten Bantul 2022</small>
        </div>
    </section>
</footer>

@extends('frontend.layouts.app')

@section('content')
    <br />
    <form methods="POST" action="{{ route('titik-assesoris.update', $model->gid) }}">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        GID <input type="text" name="gid" value="{{ $model->gid }}"><br />>
        Name <input type="text" name="name" value="{{ $model->name }}">><br />>
        Layer <input type="text" name="kml_folder" value="{{ $model->layer }}">><br />>
        Gm Type <input type="text" name="diameter" value="{{ $model->gm_type }}">><br />>
        KML Folder <input type="text" name="panjang" value="{{ $model->kml_folder }}">><br />>
        Keterangan<input type="text" name="tahun" value="{{ $model->keterangan }}">><br />>
        Geometry <input type="text" name="geom"> value="{{ $model->geom }}"><br />>


        <button type="submit">SIMPAN</button>
    </form>
@endsection

@extends('frontend.layouts.app')

@section('title')
    Jaringan_Pipa
@endsection

@section('content')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <table id="table" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>GID</th>
                                <th>Nama</th>
                                <th>KML_Layer</th>
                                <th>Diameter</th>
                                <th>Panjang</th>
                                <th>Tahun</th>
                                <th>Lokasi</th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $item->gid }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->kml_folder }}</td>
                                    <td>{{ $item->diameter }}</td>
                                    <td>{{ $item->panjang }}</td>
                                    <td>{{ $item->tahun }}</td>
                                    <td>{{ $item->lokasi_ }}</td>
                                    <td><a class="btn btn-info"
                                            href="{{ route('jaringan-pipa.edit', $item->gid) }}">update</a></td>
                                    <td>
                                        <form action="{{ route('jaringan-pipa.destroy', $item->gid) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-danger" type="submit">DELETE</button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                dom: "<'row'<'col-sm-12 col-md-5'B><'col-sm-12 col-md-4'f><'col-sm-12 col-md-3 text-right'l>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                buttons: [
                    'colvis',
                    'excel',
                    'pdf',
                    'print'
                ],
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.4/i18n/id.json'
                }
            });
        });
    </script>
@endpush

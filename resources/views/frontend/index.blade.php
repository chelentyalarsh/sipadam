@extends('frontend/layouts/app')

@section('title', 'SI-PADAM')

@section('content')
    <!--===================================Hero Section=====================================-->
    <section class="section gradient-banner">
        <div class="shapes-container">
            <div class="shape" data-aos="fade-down-left" data-aos-duration="1500" data-aos-delay="100"></div>
            <div class="shape" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100"></div>
            <div class="shape" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="200"></div>
            <div class="shape" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
            <div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
            <div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
            <div class="shape" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300"></div>
            <div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="200"></div>
            <div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="100"></div>
            <div class="shape" data-aos="zoom-out" data-aos-duration="2000" data-aos-delay="500"></div>
            <div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="200"></div>
            <div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="100"></div>
            <div class="shape" data-aos="fade-up" data-aos-duration="500" data-aos-delay="0"></div>
            <div class="shape" data-aos="fade-down" data-aos-duration="500" data-aos-delay="0"></div>
            <div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="100"></div>
            <div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="0"></div>
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 order-2 order-md-1 text-center text-md-center" style="font-size: 20px;">
                    <h1 class="text-white font-weight-bold mb-4">Sistem Informasi Pelayanan dan Inventarisasi Aset Jaringan
                        Pipa PDAM Kabupaten Bantul</h1>

                </div>
                {{-- <div class="col-md-6 text-center order-1 order-md-2">
                    <img class="img-fluid" src="{{ asset('frontend/images/Landing Page.jpg') }}" width="2500px"
                        height="2500px" alt="screenshot">
                </div> --}}
            </div>
        </div>
    </section>
    <!--====  End of Hero Section  ====-->

    <section class="section pt-0 position-relative pull-top">
        <div class="container">
            <div class="rounded shadow p-5 bg-white">
                <div class="row">
                    <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                        {{-- <i class="ti-paint-bucket text-primary h1"></i> --}}
                        <img class="" src="{{ asset('frontend/images/Pemasangan.png') }}" width="50px"
                            height="50px">
                        <h3 class="mt-4 text-capitalize h5 ">Pemasangan Baru Air PDAM</h3>
                        <p class="regular text-muted">Pemasangan baru digunakan untuk masyarakat Kabupaten Bantul yang ingin
                            memasang PDAM</p>
                        <a href={{ route('createPemasangan') }} class="btn btn-main-md"
                            style="margin-top: 15px">Daftar</a>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                        {{-- <i class="ti-shine text-primary h1"></i> --}}
                        <img class="" src="{{ asset('frontend/images/Peta.png') }}" width="50px"
                            height="50px">
                        <h3 class="mt-4 text-capitalize h5 ">Peta Jaringan Pipa PDAM</h3>

                        <p class="regular text-muted">Memberikan pengetahuan spasial kepada masyarakat Kabupaten Bantul
                            mengenai peta jaringan pipa PDAM.</p>
                        <a href="FAQ.html" class="btn btn-main-md" style="margin-top: 15px">Lihat</a>
                    </div>
                    <div class="col-lg-4 col-md-12 mt-5 mt-lg-0 text-center">
                        {{-- <i class="ti-thought text-primary h1"></i> --}}
                        <img class="" src="{{ asset('frontend/images/Pelaporan.png') }}" width="50px"
                            height="50px">
                        <h3 class="mt-4 text-capitalize h5 ">Pelaporan Gangguan Air PDAM</h3>
                        <p class="regular text-muted">Pelaporan digunakan sebagai wadah untuk melaporkan gangguan air PDAM.
                        </p>
                        <a href="FAQ.html" class="btn btn-main-md" style="margin-top: 40px">Lapor</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--==============================About ===============================-->

    <!--====  End of Services  ====-->
    <section id="about" class="service section bg-gray" style="background-image: linear-gradient()">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title" style="font-size: 50px" style="font-family: Arial, Helvetica, sans-serif">


                        <h2> Tentang Kami.</h2>
                        <p>SI-PADAM</a> merupakan aplikasi berbasis web yang berfokus pada pelayanan dan inventarisasi aset
                            PDAM Kabupaten Bantul. Sistem yang dibuat dibedakan menjadi dua, yakni pada sisi user dan admin.
                            Pemanfaatan sisi user digunakan untuk pelayanan seperti pendaftaran PDAM, pelaporan, dan peta
                            jaringan pipa PDAM. Sedangkan, pemanfaatan sisi admin dapat melakukan pengendalian manajemen
                            aset jaringan pipa PDAM Kabupaten Bantul.
                        </p>
                    </div>
                </div>
            </div>

            <section class="section testimonial" id="testimonial">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Testimonial Slider -->
                            <div class="testimonial-slider owl-carousel owl-theme">
                                <!-- Testimonial 01 -->
                                <div class="item">
                                    <div class="block shadow">
                                        <!-- Speech -->
                                        <p>
                                            Pada tahun 2019, data minat pengguna PDAM Kabupaten Bantul mengalami
                                            peningkatan. Hal
                                            tersebut akan lebih memudahkan masyarakat untuk mendaftar secara online. Selain
                                            itu, memudahkan petugas PDAM untuk memanajemen data pendaftar baru.
                                        </p>
                                        <!-- Person Thumb -->
                                        <div class="image">
                                            <img src={{ asset('frontend/images/Pemasangan.png') }} width="50px"
                                                height="50px" alt="image">
                                        </div>
                                        <!-- Name and Company -->
                                        <cite>Pemasangan Baru Air PDAM</cite>
                                    </div>
                                </div>
                                <!-- Testimonial 01 -->
                                <div class="item">
                                    <div class="block shadow">
                                        <!-- Speech -->
                                        <p>
                                            Peta jaringan pipa PDAM yang dilengkapi oleh titik aksesoris PDAM digunakan
                                            untuk
                                            edukasi
                                            spasial bagi masyarakat. Edukasi yang diberikan fokus terhadap data spasial yang
                                            ditampilkan dalam bentuk peta interaktif.
                                        </p>
                                        <!-- Person Thumb -->
                                        <div class="image">
                                            <img src={{ asset('frontend/images/Peta.png') }} alt="image">
                                        </div>
                                        <!-- Name and Company -->
                                        <cite>Peta Jaringan Pipa PDAM</cite>
                                    </div>
                                </div>
                                <!-- Testimonial 01 -->
                                <div class="item">
                                    <div class="block shadow">
                                        <!-- Speech -->
                                        <p>
                                            Laporan gangguan PDAM dapat memudahkan masyarakat untuk melaporkan gangguan yang
                                            dialami secara online dan memudahkan bagi petugas PDAM untuk mengetahui gangguan
                                            yang terjadi melalui manajemen data yang dilaporkan.
                                        </p>
                                        <!-- Person Thumb -->
                                        <div class="image">
                                            <img src={{ asset('frontend/images/Pelaporan.png') }} alt="image">
                                        </div>
                                        <!-- Name and Company -->
                                        <cite>Pelaporan Gangguan PDAM</cite>
                                    </div>
                                </div>
                                <!-- Testimonial 01 -->
                                <div class="item">
                                    <div class="block shadow">
                                        <!-- Speech -->
                                        <p>
                                            Mengoptimalkan fungsi CRUD (Create, Read, Update, Delete) dalam bentuk sistem
                                            yang membantu admin
                                            dalam
                                            memanajemen aset jaringan pipa dan titik aksesoris PDAM. Sehingga, admin
                                            memiliki kendali penuh dalam kinerja sistem SI-PADAM yang telah dibuat untuk
                                            meningkatkan peran teknologi di PDAM Kabupaten Bantul.
                                        </p>
                                        <!-- Person Thumb -->
                                        <div class="image">
                                            <img src={{ asset('frontend/images/4.png') }} alt="image">
                                        </div>
                                        <!-- Name and Company -->
                                        <cite>Inventarisasi Aset Jaringan Pipa PDAM</cite>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <!--================================= ==================================-->
            <section id="about" class="video-promo section bg-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-block"> -->
                                <!-- Heading -->
                                <h2>Lihat Video Demo SI-PADAM.</h2>
                                <!-- Promotional Speech -->
                                <p>Demo berikut termasuk pada sistem yang bekerja pada user dan admin.</p>
                                <!-- Popup Video -->
                                <a data-fancybox href="https://www.youtube.com/watch?v=jrkvirglgaQ">
                                    <i class="ti-control-play video"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--====  End of Video Promo  ====-->
        @endsection

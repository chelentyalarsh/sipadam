@extends('public.formulir')

@section('title')
    Formulir Pemasangan
@endsection

@section('content')
    <section class="container-fluid">
        <!-- justify-content-center untuk mengatur posisi form agar berada di tengah-tengah -->
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4">
                <form class="form-container">
                    <h4 class="text-center font-weight-bold">Formulir Pemasangan PDAM Baru</h4>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama" />
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor Telepon</label>
                        <input type="numeric" class="form-control" id="nomor" placeholder="Masukkan Nomor Telepon" />
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" />
                    </div>
                    <div class="form-group">
                        <label for="saluran">Saluran</label>
                        <input type="text" class="form-control" id="saluran" placeholder="Masukkan Saluran" />
                    </div>
                    <div class="form-group">
                        <label for="persil">Kegunaan Persil</label>
                        <input type="text" class="form-control" id="persil" placeholder="Kegunaan Persil" />
                    </div>
                    <div class="form-group">
                        <label for="daya">Daya</label>
                        <input type="numeric" class="form-control" id="daya" placeholder="Masukkan Daya" />
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck" />
                            <label class="form-check-label" for="gridCheck">
                                Apakah data yang diisi sudah benar?
                            </label>
                            <button type="submit" class="btn btn-primary btn-block">
                                Kirim
                            </button>
                            <!-- <div class="form-footer mt-2">
                                                  <p>Sudah punya account? <a href="#">Login</a></p>
                                                </div> -->
                </form>
            </section>
        </section>
    </section>
@endsection

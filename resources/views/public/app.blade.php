<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('frontend/libraries/bootstrap/css/bootstrap.css')}}" />
    <!-- Mendefinisikan Font diambil dari Google Fonts, dimana Font Assistant dan Playfair Display -->
    <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@200;300;400;500;600;700;800&family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />

    <!-- CSS AOS -->
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- Mendefinisikan Style Font pada Main.css -->
    <link rel="stylesheet" href="frontend/styles/main.css" />
    @stack('css')
</head>

<body>
    @include('frontend.layouts.navbar')

    @yield('content')

    @include('frontend.layouts.footer')

    <script src="{{asset('frontend/libraries/jquery/jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('frontend/libraries/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('frontend/libraries/retina/retina.min.js')}}"></script>

    <!-- JS AOS -->
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

    @stack('js')

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />

    <!-- costum css -->
    <link rel="stylesheet" href="css/style1.css" />
</head>

<body>
    <section class="container-fluid">
        <!-- justify-content-center untuk mengatur posisi form agar berada di tengah-tengah -->
        <section class="row justify-content-center" style="margin-top: 50px">
            <section class="col-12 col-sm-6 col-md-4">
                <form class="form-container">
                    <h4 class="text-center font-weight-bold">Formulir Pemasangan PDAM Baru</h4>
                    <div class="form-group" style="margin-top: 25px">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama" />
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor Telepon</label>
                        <input type="numeric" class="form-control" id="nomor" placeholder="Masukkan Nomor Telepon" />
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" />
                    </div>
                    <div class="form-group">
                        <label for="saluran">Saluran</label>
                        <input type="text" class="form-control" id="saluran" placeholder="Masukkan Saluran" />
                    </div>
                    <div class="form-group">
                        <label for="persil">Kegunaan Persil</label>
                        <input type="text" class="form-control" id="persil" placeholder="Kegunaan Persil" />
                    </div>
                    <div class="form-group">
                        <label for="daya">Daya</label>
                        <input type="numeric" class="form-control" id="daya" placeholder="Masukkan Daya" />
                    </div>
                    <div class="form-group">
                        <label for="persil">Kegunaan Persil</label>
                        <input type="numeric" class="form-control" id="persil"
                            placeholder="Masukkan Kegunaan Persil" />
                    </div>
                    <div class="form-group" style="margin: 20px">
                        <div class="form-check" style="margin-top: 20px">
                            <input class="form-check-input" type="checkbox" id="gridCheck" />
                            <label class="form-check-label" for="gridCheck" style="margin-top: ">
                                Apakah data yang diisi sudah benar?
                            </label>
                            <button type="submit" class="btn btn-primary btn-block" style="margin-top: 20px">
                                Kirim
                            </button>
                            <!-- <div class="form-footer mt-2">
              <p>Sudah punya account? <a href="#">Login</a></p>
            </div> -->
                </form>
            </section>
        </section>
    </section>

    <!-- Bootstrap requirement jQuery pada posisi pertama, kemudian Popper.js, danyang terakhit Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>

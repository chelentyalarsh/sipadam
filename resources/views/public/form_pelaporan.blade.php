<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />

    <!-- costum css -->
    <link rel="stylesheet" href="css/style1.css" />
</head>

<body>
    <section class="container-fluid">
        <!-- justify-content-center untuk mengatur posisi form agar berada di tengah-tengah -->
        <section class="row justify-content-center">
            <section class="col-12 col-sm-6 col-md-4">
                <form class="form-container">
                    <h4 class="text-center font-weight-bold">Formulir Pelaporan Gangguan Air PDAM</h4>
                    <div class="form-group">
                        <label for="saluran">Nomor Saluran Pelanggan</label>
                        <input type="numeric" class="form-control" id="saluran"
                            placeholder="Masukkan Nomor Saluran Pelanggan" />
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Pelapor</label>
                        <input type="text" class="form-control" id="pelapor" placeholder="Masukkan Nama Pelapor" />
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" />
                    </div>
                    <div class="form-group">
                        <label for="nomor">Nomor Telepon</label>
                        <input type="numeric" class="form-control" id="nomor" placeholder="Masukkan Nomor Telepon" />
                    </div>
                    <div class="form-group">
                        <label for="pelaporan">Isi Pelaporan</label>
                        <input type="text" class="form-control" id="pelaporan" placeholder="Masukkan Isi Pelaporan" />
                    </div>

                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck" />
                            <label class="form-check-label" for="gridCheck">
                                Apakah data yang diisi sudah benar?
                            </label>
                            <button type="submit" class="btn btn-primary btn-block">
                                Kirim
                            </button>
                            <!-- <div class="form-footer mt-2">
              <p>Sudah punya account? <a href="#">Login</a></p>
            </div> -->
                </form>
            </section>
        </section>
    </section>

    <!-- Bootstrap requirement jQuery pada posisi pertama, kemudian Popper.js, danyang terakhit Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
</body>

</html>

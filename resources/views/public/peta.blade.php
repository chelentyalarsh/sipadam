<!DOCTYPE html>
<html lang="en">

<head>
    <title>SI-PADAM</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-draw@1.0.4/dist/leaflet.draw.css">

    <style>
        body {
            margin: 0;
            overflow: hidden;
        }

        #map {
            height: 100vh;
            width: 100vw;
        }

    </style>
</head>

<body>
    <div id="map"></div>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdn.jsdelivr.net/npm/leaflet-draw@1.0.4/dist/leaflet.draw.min.js"></script>
    <script>
        const geojsonAksesoris = @json($layer_aksesoris);
        const geojsonUpdate = @json($layer_update);
        const map = L.map('map', {
            //layers: [basemap1, geojsonAksesoris, geojsonUpdate]
        }).setView([-7, 110], 5);

        // basemap
        var OpenStreetMap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> ǀ <a href="PJSIG UGM" target="_blank">SIG UGM</a>' //menambahkan nama//
        });

        var WorldStreetMap = L.tileLayer(
            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {});

        var WorldImagery = L.tileLayer(
            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy: Esri ǀ <a href="http://wwww.unsorry.net" target="_blank">SIG UGM</a>'
            });
        OpenStreetMap.addTo(map);


        // Layer aksesoris dan Layer update addToMap
        const layerAksesoris = L.geoJSON(geojsonAksesoris, {
            style: function(feature) {
                switch (String(feature.properties["name"])) {
                    case "Wash Out":
                        return {
                            opacity: 1,
                                color: "#FF0000",
                                weight: 1,
                        };
                        break;
                    case "Cross":
                        return {
                            opacity: 1,
                                color: "#FFC349",
                                weight: 1,
                        };
                        break;
                    case "Gate Valve":
                        return {
                            opacity: 1,
                                color: "#86E929",
                                weight: 1,
                        };
                        break;
                    case "Intake":
                        return {
                            opacity: 1,
                                color: "#00A0E9",
                                weight: 1,
                        };
                        break;
                    case "Reservoir":
                        return {
                            opacity: 1,
                                color: "#E36C09",
                                weight: 1,
                        };
                        break;
                    case "Stop Keran":
                        return {
                            opacity: 1,
                                color: "#D33249",
                                weight: 1,
                        };
                        break;
                    case "CAP":
                        return {
                            opacity: 1,
                                color: "#D33249",
                                weight: 1,
                        };
                        break;
                    case "Mata Air":
                        return {
                            opacity: 1,
                                color: "#D33249",
                                weight: 1,
                        };
                        break;
                    case "Node":
                        return {
                            opacity: 1,
                                color: "#D33249",
                                weight: 1,
                        };
                        break;
                    case "SB":
                        return {
                            opacity: 1,
                                color: "#D33249",
                                weight: 1,
                        };
                        break;
                }
            },
            onEachFeature: function(feature, layer) {
                if (feature.properties) {
                    var popupContent =
                        '<table class="table table-striped table-condensed">' +
                        '<tr style="padding: 0.1rem"><th style="padding: 0.1rem">Nama Aksesoris </th><td style="padding: 0.1rem"> : ' +
                        feature.properties["name"] +
                        "</td></tr>" +
                        '<tr style="padding: 0.1rem"><th style="padding: 0.1rem">Keterangan </th><td style="padding: 0.1rem"> : ' +
                        feature.properties["THICKNESS"] +
                        "</td></tr>" +
                        "</table>";

                    layer.bindPopup(popupContent, {
                        minWidth: 50,
                    });
                    // layer.on({
                    //     mouseover: function(e) {
                    //         var layer = e.target;
                    //         layer.setStyle({
                    //             weight: 4,
                    //         });
                    //     },
                    //     mouseout: function(e) {
                    //         layerAksesoris.resetStyle(e.target);
                    //     },
                    // });
                }
            },
        });
        layerAksesoris.addTo(map);

        const layerUpdate = L.geoJSON(geojsonUpdate).addTo(map);
        map.fitBounds(layerUpdate.getBounds());

        var basemap = {
            'Open Street Map': OpenStreetMap,
            'World Street Map': WorldStreetMap,
            'World Imagery': WorldImagery
        };
        var layer = {
            'Titik Aksesoris': layerAksesoris,
            'Jaringan Pipa': layerUpdate
        };
        L.control.layers(basemap, layer).addTo(map);



        var drawControl = new L.Control.Draw({
            edit: {
                featureGroup: layerAksesoris
            }
        });
        map.addControl(drawControl);

        map.on('draw:created', (e) => {
            const geojson = e.layer.toGeoJSON();
            const geom = geojson.geometry;
            console.log(geom);
        });
    </script>
</body>

</html>
